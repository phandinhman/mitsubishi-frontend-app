import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory'
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import { createStore, applyMiddleware, compose } from "redux"
import rootReducer from "./reducers/index";

import Home from './components/Home';
import CarDetail from "./components/CarDetail";
import CarByCategory from "./components/CarByCategory";
import Signup from "./components/Signup";
import Signin from "./components/Signin";

import './style/index.css';
import './style/bootstrap.min.css';
import './style/font-awesome.scss';
import './style/jgrowl.scss';
import './style/login.scss';
import './style/multi-columns-row.scss';
import './style/owl.carousel.scss';
import './style/revslider.scss';


const history = createHistory()

const store = createStore(
  rootReducer,
  compose(
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
)

ReactDOM.render(

  <Provider store={store}>
    <BrowserRouter history={history}>
      <div>
        <Route exact path="/" component={Home} />
        <Route path="/cars/:slug" render={(renderProps) => (
                                    <div>
                                      <CarDetail slug={renderProps.match.params.slug} />
                                    </div>
                                  )} />
        <Route path="/cartegories/:slug" render={(renderProps) => (
                                    <div>
                                      <CarByCategory slug={renderProps.match.params.slug} />
                                    </div>
                                  )} />
        <Route path="/gioi-thieu" component={Home} />
        <Route path="/login" component={Signin} />
        <Route path="/register" component={Signup} />

      </div>
    </BrowserRouter>
  </Provider>, document.getElementById('app')
  );

try {
} catch (e) {
}
