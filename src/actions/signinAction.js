import axios from "axios";

export function usersSiginRequest(user) {
  return dispatch => {
    return axios.post("https://mitsubishi.herokuapp.com/api/v1/sessions", user).then(res => {
      const token = res.data.mitsubishi.data.token;
      console.log(token);
      localStorage.setItem("jwtToken", token);
    });

  }
}