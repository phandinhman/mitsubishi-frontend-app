import axios from "axios";
import setAuthorizationToken from "./../utils/setAuthorizationToken";
import jwtDecode from "jwt-decode";
import * as types from './../constants/actionType';

export function setCurrentUser(user) {
    return {
        type: types.SET_CURRENT_USER,
        user
    }
}

export function loginSuccess(settings) {
  return function (dispatch) {
    return dispatch({
      type: types.LOGIN_SUCCESS,
      settings
    });
  };
}

export function logout() {
  return dispatch => {
    localStorage.removeItem('jwtToken');
    setAuthorizationToken(false);
    dispatch(setCurrentUser({}));
  };
}

export function loginFailure(settings) {
  return {
    type: types.LOGIN_FAILURE,
    settings
  };
}

export function login(data) {
    return dispatch => {
        return axios.post("https://mitsubishi.herokuapp.com/api/v1/sessions").then(res => {
            const token = res.data.mitsubishi.data.token;
            setAuthorizationToken(token);
            localStorage.setItem("jwtToken", token);
            dispatch(setCurrentUser(jwtDecode(token)));
        });
    }
}