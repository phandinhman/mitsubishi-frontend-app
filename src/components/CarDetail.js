import React from "react";
import $ from "jquery";

import Header from "./Header";
import Car from "./Car";
import Footer from "./Footer"

class CarDetail extends React.Component {

  componentDidMount() {
    $.getJSON("https://mitsubishi.herokuapp.com/api/v1/cars", (response) => {this.setState({cars: response.mitsubishi.data})});

  }

  render() {
    const id = this.props.slug;
    console.log(id);
    return(
      <div>
        <Header />
        <section className="main-container col1-layout home-content-container">
          <div className="container">
            <div className="std row">
              <Car />
            </div>
          </div>
        </section>
        <Footer />
      </div>
    )
  }
}

export default CarDetail;
