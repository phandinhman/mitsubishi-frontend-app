import React from "react";
import $ from "jquery";

import { Link } from "react-router-dom"

import Category from "./Category"

class Categories extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: [],
    }
  }

  componentDidMount() {
    console.debug('componentDidMount');
    $.getJSON("https://mitsubishi.herokuapp.com//api/v1/categories", (response) => {this.setState({categories: response.mitsubishi.data})});
  }

  render() {
    const categories = this.state.categories;
    const categoryList = categories.map((category, index) => {
      return (
        <Link to={"/cartegories/" + category.slug} key={category.id}><Category name={category.name}  key={category.id} id={category.id}/></Link>
      )
    });
    return(
      <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <div className="box_collection_pr">
          <div className="title_st">
            <h2>Danh mục sản phẩm</h2>
            <span className="arrow_title visible-md visible-md"></span>
            <div className="show_nav_bar hidden-lg hidden-md"></div>
          </div>
          <div className="list_item">
            <div className="product-item-slide multi-columns-row">
              <ul id="categories">
                  {categoryList}
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Categories;
