import React from "react";

class Footer extends React.Component {
  render() {
    return(
      <footer className="footer">
         <div className="footer-middle container">
            <div className="row">
               <div className="col-md-4 col-sm-4">
                  <h2 className="h2_footer">Về chúng tôi</h2>
                  <span className="sp_footer"></span>
                  <div className="footer-logo">
                     <a className="logo" href="//thegioimitsubishi.com">
                     <img alt="Thế giới Mitsubishi, xe mitsubishi, đại lý mitsubishi, mitsubishi attrage, mitsubishi pajero sport, mitsubishi outlander, mitsubishi pajero sport, mitsubishi triton, mitsubishi mirage" src="//bizweb.dktcdn.net/100/176/403/themes/231146/assets/logo_footer.png?1495680917512" />
                     </a>
                  </div>
                  <p>Thế giới Mitsubishi được thành lập với niềm đam mê và khát vọng thành công trong lĩnh vực xe hơi. Chúng tôi đã và đang khẳng định được vị trí hàng đầu bằng những sản phẩm tốt nhất.
                  </p>
               </div>
               <div className="col-md-2 col-sm-2">
                  <h2 className="h2_footer">Thông tin</h2>
                  <span className="sp_footer"></span>
                  <ul className="links">
                     <li><a href="/">Trang chủ</a></li>
                     <li><a href="/collections/all">Sản phẩm</a></li>
                     <li><a href="/tin-tuc">Tin tức</a></li>
                     <li><a href="/lien-he">Liên hệ</a></li>
                  </ul>
               </div>
               <div className="col-md-3 col-sm-3">
                  <h2 className="h2_footer">Điều khoản dịch vụ</h2>
                  <span className="sp_footer"></span>
                  <ul className="links">
                     <li><a href="/dieu-khoan">Điều khoản sử dụng</a></li>
                     <li><a href="/dieu-khoan">Điều khoản giao dịch</a></li>
                     <li><a href="/dieu-khoan">Dịch vụ tiện ích</a></li>
                     <li><a href="/dieu-khoan">Quyền sở hữu trí tuệ</a></li>
                  </ul>
               </div>
               <div className="col-md-3 col-sm-3">
                  <h2 className="h2_footer">Liên hệ</h2>
                  <span className="sp_footer"></span>
                  <div className="contacts-info">
                     <address><span className="glyphicon glyphicon-map-marker"></span>Km12 – Tổ 15 – Phường Phúc Diễn – Bắc Từ Liêm – Hà Nội</address>
                     <div className="phone-footer"><span className="glyphicon glyphicon-earphone"></span> &nbsp; 097 999 5130</div>
                     <div className="phone-footer"><i className="fa fa-archive"></i> &nbsp;0948 716 386</div>
                     <div className="email-footer"><span className="glyphicon glyphicon-envelope"></span> &nbsp;<a href="mailto: Email : Luctv@hanoiauto.com.vn"> Email : Luctv@hanoiauto.com.vn</a> </div>
                  </div>
                  <div className="payment-accept">
                     <div>
                        <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/fizz.legend"><img src="//bizweb.dktcdn.net/100/176/403/themes/231146/assets/payment-1.png?1495680917512" alt="payment" /></a>
                        <a target="_blank" rel="noopener noreferrer" href="#"><img src="//bizweb.dktcdn.net/100/176/403/themes/231146/assets/payment-2.png?1495680917512" alt="payment" /></a>
                        <a target="_blank" rel="noopener noreferrer" href=""><img src="//bizweb.dktcdn.net/100/176/403/themes/231146/assets/payment-3.png?1495680917512" alt="payment" /></a>
                        <a target="_blank" rel="noopener noreferrer" href="#"><img src="//bizweb.dktcdn.net/100/176/403/themes/231146/assets/payment-4.png?1495680917512" alt="payment" /></a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div className="footer-bottom">
            <div className="container">
               <div className="row">
                  <div className="col-sm-5 col-xs-12 coppyright">
                     © Bản quyền thuộc về Thế giới Mitsubishi, xe mitsubishi, đại lý mitsubishi, mitsubishi attrage, mitsubishi pajero sport, mitsubishi outlander, mitsubishi pajero sport, mitsubishi triton, mitsubishi mirage | Cung cấp bởi <a href="https://www.bizweb.vn/?utm_source=site-khach-hang&amp;utm_campaign=referral_bizweb&amp;utm_medium=footer&amp;utm_content=cung-cap-boi-bizweb" target="_blank" title="Bizweb">Bizweb</a>
                  </div>
                  <div className="col-sm-7 col-xs-12 company-links hidden-xs">
                     <a href="/"><img src="//bizweb.dktcdn.net/100/176/403/themes/231146/assets/icon_visa.png?1495680917512" alt="thanh-toan" /></a>
                  </div>
               </div>
            </div>
         </div>
      </footer>
    );
  }
}

export default Footer;
