import React from "react";
import { connect } from 'react-redux';
import createHistory from 'history/createBrowserHistory';
import PropTypes from 'prop-types';

import { login } from "../actions/authActions"

export const history = createHistory()


class SigninForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      errors: {},
      isLoading: false
    }
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    this.setState({[e.target.name]: e.target.value});
  }

  onSubmit(e) {
    e.preventDefault();

    const { login } = this.props;
    if (login) {
      const { email, password } = this.state;
      login({email, password});
    }
  }

  render() {

    const { email, password } = this.state;

    return(
      <div className="main container">
       <div className="account-login">
          <div className="page-title">
             <h2>Đăng nhập</h2>
          </div>
          <fieldset className="col2-set">
             <legend>Đăng nhập / tạo tài khoản</legend>
             <div className="col-2 registered-users">
                <strong>Đăng nhập</strong>
                <div className="content">
                    <p>Nếu bạn đã có tài khoản, xin vui lòng đăng nhập </p>
                    <form onSubmit={this.onSubmit}>
                      <ul className="form-list">
                         <li>
                            <label>Email <span className="required">*</span></label>
                            <br/>
                            <input type="email" value= {email} onChange={this.onChange} title="Email Address" className="input-text required-entry" id="email" name="email"/>
                         </li>
                         <li>
                            <label>Mật khẩu <span className="required">*</span></label>
                            <br/>
                            <input type="password" value= {password} onChange={this.onChange} title="Password" id="pass" className="input-text required-entry validate-password" name="password"/>
                         </li>
                      </ul>
                      <p className="required">* Yêu cầu bắt buộc</p>
                      <div className="buttons-set">
                         <button id="send2" name="send" type="submit" className="button login"><span>Đăng nhập</span></button>
                         <a className="forgot forgot-word" href="#recover" id="RecoverPassword">Quên mật khẩu?</a>
                      </div>
                    </form>
                </div>
             </div>
             <div className="col-1 new-users">
                <strong>Khách hàng mới</strong>
                <div className="content">
                   <p>Bằng cách tạo một tài khoản với cửa hàng của chúng tôi, bạn sẽ có thể thực hiện những quy trình mua hàng nhanh hơn, lưu trữ nhiều địa chỉ gửi hàng, xem và theo dõi đơn đặt hàng của bạn trong tài khoản của bạn và nhiều hơn nữa.</p>
                   <div className="buttons-set">
                      <button className="button create-account" type="button"><span>Tạo tài khoản</span></button>
                   </div>
                </div>
             </div>
          </fieldset>
       </div>
       <br/>
     </div>
    )
  }
}

const mapStateToProps = (state) => {
  const { isAuthenticated } = state.auth;
  return {
    isAuthenticated
  };
}

export default connect(mapStateToProps, {login})(SigninForm);
