import React from "react";

class SigninForm extends React.Component {
  render() {
		return(
      <fieldset className="col2-set register_styl">
				<legend>Thông tin cá nhân</legend>
				<div className="col-1 new-users">
					<div className="content">
						<form  action="/account/register" id="customer_register" method="post">
              <input name="FormType" type="hidden" value="customer_register"/>
              <input name="utf8" type="hidden" value="true" />
							<div className="form-group">
								<label>Họ<span id="required">*</span></label>
								<input type="text" name="LastName" id="last_name" className="form-control" placeholder="Họ" />
							</div>
							<div className="form-group form-group-right">
								<label>Tên<span id="required">*</span></label>
								<input type="text" className="form-control" name="FirstName" id="first_name" placeholder="Tên" />
							</div>
							<div className="form-group">
								<label>Email<span id="required">*</span></label>
								<input type="text" className="form-control" name="email" id="email" placeholder="Email" />
							</div>
							<div className="form-group form-group-right">
								<label>Mật khẩu<span id="required">*</span></label>
								<input  type="password" className="form-control" name="password" id="creat_password" placeholder="Mật khẩu" />
							</div>
							<div className="fvc">
								<p>Những trường <span id="required">*</span> là trường bắt buộc</p>
								<button type="submit" className="btn btn-default button-red">Đăng ký</button>
								<a href="/account/login"></a>
								<button type="submit" className="btn btn-default button-red"><a href="/account/login"></a><a href="/">Quay lại</a></button>
							</div>

						</form>
					</div>
				</div>
			</fieldset>
    )
  }
}

export default SigninForm;
