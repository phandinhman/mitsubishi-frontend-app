import React from "react";
import Header from "./Header";
import Categories from "./Categories";
import Cars from "./Cars";
import Footer from "./Footer"

class App extends React.Component {
  render() {
    return(
      <div>
        <Header />
        <section className="main-container col1-layout home-content-container">
          <div className="container">
            <div className="std row">
              <Categories />
              <Cars />
            </div>
          </div>
        </section>
        <Footer />
      </div>
    )
  }
}

export default App;
