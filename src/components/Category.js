import React from "react";

class Category extends React.Component {
  render() {
    return(
      <li>
        <a>{this.props.name}</a>
      </li>
    )
  }
}

export default Category;
