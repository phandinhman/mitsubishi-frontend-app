import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import Footer from "./Footer"
import Header from "./Header";
import SigninForm from "./SigninForm"

import { usersSiginRequest } from "../actions/signinAction"

class Signin extends React.Component {
  render() {

    const { usersSiginRequest } = this.props;

    return(
      <div>
        <Header />
        <section className="main-container col1-layout home-content-container">
          <div className="container">
            <div className="std row">
              <SigninForm usersSiginRequest={usersSiginRequest}/>
            </div>
          </div>
        </section>
        <Footer />
      </div>
    )
  }
}

Signin.propTypes = {
  usersSiginRequest: PropTypes.func.isRequired,
}

export default connect((state) => {return {} }, {usersSiginRequest})(Signin)
