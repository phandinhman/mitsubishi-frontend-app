import React from "react";
import { Link } from "react-router-dom";

import Navbar from "./Navbar"

class Header extends React.Component {
  render() {
    return(
      <div>
        <header className="header-container">
          <div className="header-top">
            <div className="container">
               <div className="row">
                  <div className="col-sm-8 col-xs-12">
                     <div className="welcome-msg hidden-xs">Chào mừng bạn tới <a>Thế giới xe mitsubishi</a> | Hotline : <a>0905136174</a></div>
                     <div className="account-msg visible-xs">
                        <a className="msg">Tài khoản <i className="fa fa-angle-down"></i></a>
                        <ul className="account-msg-hidden">
                           <li><a title="Register" href="/account/register"><span><i className="fa fa-key"></i>Đăng ký</span></a></li>
                           <li><a title="Login" href="/account/login"><span><i className="fa fa-lock"></i>Đăng nhập</span></a></li>
                        </ul>
                     </div>
                  </div>
                  <div className="col-sm-4 col-xs-12">
                     <div className="toplinks">
                        <div className="links hidden-xs">
                           <div className="register">
                              <Link to="/login">
                                <span className="hidden-xs">Đăng nhập</span>
                              </Link>
                              <Link to="/register">
                                <span  className="hidden-xs">Đăng ký</span>
                                </Link>
                              </div>
                        </div>
                        <div className="links visible-xs">
                           <div><a title="search" href="/search"><span className="fa fa-search"></span></a></div>
                           <div className="bag_shop"><a title="cart" href="/cart"><span className="cart-total">0</span></a></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
          </div>
          <div className="header container">
            <div className="row">
               <div className="col-lg-4 col-sm-4 col-md-4 cl_logo">
                  <Link to="/">
                     <img alt="Thế giới Mitsubishi, xe mitsubishi, đại lý mitsubishi, mitsubishi attrage, mitsubishi pajero sport, mitsubishi outlander, mitsubishi pajero sport, mitsubishi triton, mitsubishi mirage" src="//bizweb.dktcdn.net/100/176/403/themes/231146/assets/logo.png?1494382782936" />
                     <p className="sl-txt">mitsubishi motor</p>
                  </Link>
               </div>
               <div className="col-lg-5 col-sm-5 col-md-5">
                  <div className="search-box">
                     <form action="/search" method="get" id="search_mini_form">
                        <input type="text" placeholder="Nhập thông tin tìm kiếm" value="" className="" name="query" id="search" />
                        <button type="submit" id="submit-button" className="search-btn-bg"><span></span></button>
                     </form>
                  </div>
               </div>
               <div className="col-lg-3 col-sm-3 col-md-3">
                  <div className="top-cart-contain">
                     <div className="mini-cart" id="open_shopping_cart">
                        <div data-toggle="dropdown" data-hover="dropdown" className="basket dropdown-toggle">
                           <a href="/cart">
                              <i className="icon_bag"><span className="count_item_pr cart-total">0</span></i>
                              <div className="cart-box"><span className="title">Giỏ hàng</span><span className="count_pr">(<span className="cart-total">0</span>)&nbsp;sản phẩm</span></div>
                           </a>
                        </div>
                        <div>
                           <div className="top-cart-content arrow_box">
                              <ul id="cart-sidebar" className="mini-products-list shopping_cart">
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div id="ajaxconfig_info"> <a href="#/"></a>
                        <input value="" type="hidden" />
                        <input id="enable_module" value="1" type="hidden" />
                        <input className="effect_to_cart" value="1" type="hidden" />
                        <input className="title_shopping_cart" value="Giỏ hàng" type="hidden" />
                     </div>
                  </div>
               </div>
            </div>
          </div>
        </header>
        <Navbar/>
      </div>
    )
  }
}

export default Header;
