import React from "react";

class Navbar extends React.Component {
  render() {
    return(
      <nav>
        <div className="container">
          <div className="nav-inner">
             <div className="logo-small hidden-sm hidden-md">
                <a>
                  <img alt="Thế giới Mitsubishi, xe mitsubishi, đại lý mitsubishi, mitsubishi attrage, mitsubishi pajero sport, mitsubishi outlander, mitsubishi pajero sport, mitsubishi triton, mitsubishi mirage" src="//bizweb.dktcdn.net/100/176/403/themes/231146/assets/logo.png?1494382782936" />
                </a>
             </div>
             <div className="hidden-desktop" id="mobile-menu">
                <ul className="navmenu">
                   <li>
                      <div className="menutop toggle">
                         <div className="toggle toggle_icon"> <span className="icon-bar"></span> <span className="icon-bar"></span> <span className="icon-bar"></span></div>
                         <h2>Danh mục</h2>
                      </div>
                      <ul className="submenu">
                         <li>
                            <ul className="topnav">
                               <li className="level0 nacvv-8 level-top parent  active "> <a className="level-top" href="/"> <span>Trang chủ</span> </a></li>
                               <li className="level0 nacvv-8 level-top parent "> <a className="level-top" href="/gioi-thieu"> <span>Giới thiệu</span> </a></li>
                               <li className="level0 nav-8 level-top parent ">
                                  <a className="level-top" href="/collections/all"> <span>Sản phẩm</span> </a>
                                  <ul className="level0">
                                     <li className="level1 nav-2-1 first parent "> <a href="/xe-mitsubishi-moi"> <span>SẢN PHẨM MỚI</span> </a></li>
                                     <li className="level1 nav-2-1 first parent "> <a href="/xe-o-to-mitsubishi-khuyen-mai"> <span>SẢN PHẨM KHUYẾN MẠI</span> </a></li>
                                     <li className="level1 nav-2-1 first parent "> <a href="/san-pham-xe-mitsubishi-ban-chay"> <span>SẢN PHẨM BÁN CHẠY</span> </a></li>
                                     <li className="level1 nav-2-1 first parent "> <a href="/attrage"> <span>ATTRAGE</span> </a></li>
                                     <li className="level1 nav-2-1 first parent "> <a href="/mirage"> <span>MIRAGE</span> </a></li>
                                     <li className="level1 nav-2-1 first parent "> <a href="/outlander"> <span>OUTLANDER</span> </a></li>
                                     <li className="level1 nav-2-1 first parent "> <a href="/outlander-sport"> <span>OUTLANDER SPORT</span> </a></li>
                                     <li className="level1 nav-2-1 first parent "> <a href="/pajero"> <span>PAJERO</span> </a></li>
                                     <li className="level1 nav-2-1 first parent "> <a href="/pajero-sport"> <span>PAJERO SPORT</span> </a></li>
                                     <li className="level1 nav-2-1 first parent "> <a href="/all-new-pajero-sport"> <span>ALL NEW PAJERO SPORT</span> </a></li>
                                     <li className="level1 nav-2-1 first parent "> <a href="/triton"> <span>TRITON</span> </a></li>
                                  </ul>
                               </li>
                               <li className="level0 nav-8 level-top parent ">
                                  <a className="level-top" href="/mua-xe"> <span>Mua xe</span> </a>
                                  <ul className="level0">
                                     <li className="level1 nav-2-1 first parent "> <a href="/chuong-trinh-khuyen-mai-xe-mitsubishi"> <span>Chương trình khuyến mại xe</span> </a></li>
                                     <li className="level1 nav-2-1 first parent "> <a href="/bang-gia"> <span>Bảng giá</span> </a></li>
                                     <li className="level1 nav-2-1 first parent "> <a href="/tim-dai-ly"> <span>Tìm đại lý</span> </a></li>
                                     <li className="level1 nav-2-1 first parent "> <a href="/dang-ky-lai-thu"> <span>Đăng kí lái thử</span> </a></li>
                                     <li className="level1 nav-2-1 first parent "> <a href="/yeu-cau-bao-gia"> <span>Yêu cầu báo giá</span> </a></li>
                                  </ul>
                               </li>
                               <li className="level0 nav-8 level-top parent ">
                                  <a className="level-top" href="/dich-vu"> <span>Dịch vụ</span> </a>
                                  <ul className="level0">
                                     <li className="level1 nav-2-1 first parent "> <a href="/gioi-thieu-dich-vu"> <span>Giới thiệu dịch vụ</span> </a></li>
                                     <li className="level1 nav-2-1 first parent "> <a href="/dai-ly-dich-vu"> <span>Đại lý dịch vụ</span> </a></li>
                                     <li className="level1 nav-2-1 first parent "> <a href="/chinh-sach-bao-hanh"> <span>Chính sách bảo hành</span> </a></li>
                                     <li className="level1 nav-2-1 first parent "> <a href="/khao-sat-khach-hang"> <span>Khảo sát khách hàng</span> </a></li>
                                  </ul>
                               </li>
                               <li className="level0 nav-8 level-top parent ">
                                  <a className="level-top" href="/tin-tuc"> <span>Tin tức</span> </a>
                                  <ul className="level0">
                                     <li className="level1 nav-2-1 first parent "> <a href="/chuong-trinh-khuyen-mai-xe-mitsubishi"> <span>Tin khuyến mại</span> </a></li>
                                  </ul>
                               </li>
                               <li className="level0 nacvv-8 level-top parent "> <a className="level-top" href="/lien-he"> <span>Liên hệ</span> </a></li>
                            </ul>
                         </li>
                      </ul>
                   </li>
                </ul>
             </div>
             <ul id="nav" className="hidden-xs">
                <li className="level0 parent drop-menu  active "><a href="/"><span>Trang chủ</span></a></li>
                <li className="level0 parent drop-menu "><a href="/gioi-thieu"><span>Giới thiệu</span></a></li>
                <li className="level0 parent drop-menu ">
                   <a href="/collections/all"><span>Sản phẩm <i className="fa fa-sort-desc"></i></span></a>
                   <ul className="level1">
                      <i className="fa fa-sort-asc icon_arr"></i>
                      <li className="level1 first parent "><a href="/xe-mitsubishi-moi"><span>SẢN PHẨM MỚI</span></a></li>
                      <li className="level1 first parent "><a href="/xe-o-to-mitsubishi-khuyen-mai"><span>SẢN PHẨM KHUYẾN MẠI</span></a></li>
                      <li className="level1 first parent "><a href="/san-pham-xe-mitsubishi-ban-chay"><span>SẢN PHẨM BÁN CHẠY</span></a></li>
                      <li className="level1 first parent "><a href="/attrage"><span>ATTRAGE</span></a></li>
                      <li className="level1 first parent "><a href="/mirage"><span>MIRAGE</span></a></li>
                      <li className="level1 first parent "><a href="/outlander"><span>OUTLANDER</span></a></li>
                      <li className="level1 first parent "><a href="/outlander-sport"><span>OUTLANDER SPORT</span></a></li>
                      <li className="level1 first parent "><a href="/pajero"><span>PAJERO</span></a></li>
                      <li className="level1 first parent "><a href="/pajero-sport"><span>PAJERO SPORT</span></a></li>
                      <li className="level1 first parent "><a href="/all-new-pajero-sport"><span>ALL NEW PAJERO SPORT</span></a></li>
                      <li className="level1 first parent "><a href="/triton"><span>TRITON</span></a></li>
                   </ul>
                </li>
                <li className="level0 parent drop-menu ">
                   <a href="/mua-xe"><span>Mua xe <i className="fa fa-sort-desc"></i></span></a>
                   <ul className="level1">
                      <i className="fa fa-sort-asc icon_arr"></i>
                      <li className="level1 first parent "><a href="/chuong-trinh-khuyen-mai-xe-mitsubishi"><span>Chương trình khuyến mại xe</span></a></li>
                      <li className="level1 first parent "><a href="/bang-gia"><span>Bảng giá</span></a></li>
                      <li className="level1 first parent "><a href="/tim-dai-ly"><span>Tìm đại lý</span></a></li>
                      <li className="level1 first parent "><a href="/dang-ky-lai-thu"><span>Đăng kí lái thử</span></a></li>
                      <li className="level1 first parent "><a href="/yeu-cau-bao-gia"><span>Yêu cầu báo giá</span></a></li>
                   </ul>
                </li>
                <li className="level0 parent drop-menu ">
                   <a href="/dich-vu"><span>Dịch vụ <i className="fa fa-sort-desc"></i></span></a>
                   <ul className="level1">
                      <i className="fa fa-sort-asc icon_arr"></i>
                      <li className="level1 first parent "><a href="/gioi-thieu-dich-vu"><span>Giới thiệu dịch vụ</span></a></li>
                      <li className="level1 first parent "><a href="/dai-ly-dich-vu"><span>Đại lý dịch vụ</span></a></li>
                      <li className="level1 first parent "><a href="/chinh-sach-bao-hanh"><span>Chính sách bảo hành</span></a></li>
                      <li className="level1 first parent "><a href="/khao-sat-khach-hang"><span>Khảo sát khách hàng</span></a></li>
                   </ul>
                </li>
                <li className="level0 parent drop-menu ">
                   <a href="/tin-tuc"><span>Tin tức <i className="fa fa-sort-desc"></i></span></a>
                   <ul className="level1">
                      <i className="fa fa-sort-asc icon_arr"></i>
                      <li className="level1 first parent "><a href="/chuong-trinh-khuyen-mai-xe-mitsubishi"><span>Tin khuyến mại</span></a></li>
                   </ul>
                </li>
                <li className="level0 parent drop-menu "><a href="/lien-he"><span>Liên hệ</span></a></li>
             </ul>
          </div>
       </div>
    </nav>
    );
  }
}

export default Navbar;
