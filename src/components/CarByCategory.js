import React from "react";
import $ from "jquery";
import { Link } from "react-router-dom"
import Header from "./Header";
import Categories from "./Categories";
import Footer from "./Footer"

import Car from "./Car"

class CarByCategory extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      cars: [],
    }
  }

  componentDidMount() {
    const slug = this.props.slug;
    $.getJSON("https://mitsubishi.herokuapp.com/api/v1/categories/" + slug, (response) => {this.setState({cars: response.mitsubishi.data})});
  }

  componentWillReceiveProps(nextProps) {
    const slug = nextProps.slug;
    $.getJSON("https://mitsubishi.herokuapp.com/api/v1/categories/" + slug, (response) => {this.setState({cars: response.mitsubishi.data})});
  }

  render() {
    const cars = this.state.cars;
    const carsList = cars.map((car, index) => {
      return (
          <Link to={"/cars/" + car.slug} key={car.id} data={car}><Car name={car.name} cost={car.cost} special_cost={car.special_cost} key={car.id} id={car.i}/></Link>
        )
    });
    return(
      <div>
        <Header />
        <section className="main-container col1-layout home-content-container">
          <div className="container">
            <div className="std row">
              <Categories />
              <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div className="best-seller-pro">
                  <div className="slider-items-products">
                    <div className="new_title center">
                      <h2>Sản phẩm khuyến mại</h2>
                    </div>
                    <div id="bag-seller-slider" className="product-flexslider hidden-buttons">
                      <div className="slider-items slider-width-col4 owl-carousel owl-theme">
                        <div className="owl-wrapper-outer autoHeight">
                          <div className="owl-wrapper">
                            <div className="owl-item">
                              <div className="product-item-slide multi-columns-row">
                                {carsList}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Footer />
      </div>

    );
  }

}

export default CarByCategory;
