import React from "react";

import Footer from "./Footer"
import Header from "./Header";
import SignupForm from "./SignupForm"

class Signup extends React.Component {
  render() {
    return(
      <div>
        <Header />
        <section className="main-container col1-layout home-content-container">
          <div className="container">
            <div className="std row">
              <SignupForm />
            </div>
          </div>
        </section>
        <Footer />
      </div>
    )
  }
}

export default Signup;
